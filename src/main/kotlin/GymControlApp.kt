import java.util.*

interface GymControlReader {
    fun nextId(): String
}

class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId() = scanner.next()
}

fun main() {
    val reader: GymControlReader = GymControlManualReader()
    val enteredUsers = HashMap<String, Boolean>() // HashMap per mantenir el seguiment dels usuaris que han entrat
    while(true){
        try {
            val id = reader.nextId()
            if (enteredUsers[id] == null) { // l'usuari no ha entrat abans
                enteredUsers[id] = true
                println("$id Entrada")
            } else { // l'usuari ja ha entrat abans
                enteredUsers.remove(id)
                println("$id Sortida")
            }
        } catch (error: Exception) {
            break
        }
    }
}