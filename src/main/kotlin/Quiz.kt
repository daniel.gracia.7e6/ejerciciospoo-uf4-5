open class Question(open val question: String) {
    open fun answerQuestion (): Boolean {
        return false
    }
}

class FreeTextQuestion(question: String, val correctAnswer: String): Question(question) {
    override fun answerQuestion(): Boolean {
        println(question)
        println("Your answer:")
        val answer = readln()

        if(answer == correctAnswer) {
            println("WELL DONE")
            return true
        } else {
            println("NOT GOOD")
            return false
        }
    }
}

class MultipleChoiseQuestion(val choiceList: Array<String>, val correctChoice: Int, question: String): Question(question) {
    override fun answerQuestion(): Boolean {
        println(question)
        for(i in 1..choiceList.size) {
            println("$i - ${choiceList[i-1]}")
        }
        val answer = readln().toInt()

        if(answer == correctChoice) {
            println("WELL DONE")
            return true
        } else {
            println("NOT GOOD")
            return false
        }
    }
}

class Quiz(val questions: Array<Question>) {
    private var correctAnswerCounter: Int = 0

    fun playQuiz() {
        for(question in questions) {
            if(question.answerQuestion()) {
                correctAnswerCounter += 1
            }
        }
    }

    fun showStats() {
        println("""
            |** GAME END **
            |Correct answers: $correctAnswerCounter
            |Bad answers: ${questions.size - correctAnswerCounter}
            |Success Rate: ${(correctAnswerCounter.toFloat() / questions.size.toFloat()) * 100}%
        """.trimMargin())
    }
}

fun main() {
    val myQuiz = Quiz(arrayOf(
        FreeTextQuestion("Capital from Spain", "Madrid"),
        MultipleChoiseQuestion(arrayOf(
            "Spain",
            "Italy",
            "England",
            "Rusia"
        ),1,"Who won the football world cup in 2010"),
        MultipleChoiseQuestion(arrayOf(
            "San Martin",
            "Napoleon",
            "Richard Kingman",
            "Cristobal Colon"
        ),4,"Who discovered America"),
        FreeTextQuestion("Capital from South Africa", "Sun City"),
    ))

    myQuiz.playQuiz()
    myQuiz.showStats()
}