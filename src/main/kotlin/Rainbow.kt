fun main() {
    val coloresArcoiris: Array<String> = arrayOf("red", "orange", "yellow", "green", "blue", "indigo", "violet")
    val color = readln()

    if(color in coloresArcoiris) {
        println(true)
    } else {
        println(false)
    }
}