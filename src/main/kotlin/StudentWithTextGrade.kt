class Student(val name: String, val textGrade: String) {
    override fun toString(): String {
        return "Student{name='$name', textGrade=$textGrade}"
    }
}

fun main() {
    val student1 = Student("Mar", "FAILED")
    val student2 = Student("Joan", "EXCELLENT")
    println(student1)
    println(student2)
}