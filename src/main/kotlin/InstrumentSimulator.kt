open class Instrument() {
    open fun makeSounds(repeticiones: Int) {}
}

class Triangle(val resonancia: Int): Instrument() {
    override fun makeSounds(repeticiones: Int) {
        for(i in 0..repeticiones) {
            println("T${"I".repeat(resonancia)}NC")
        }
    }
}

class Drump(val tono: String): Instrument() {
    override fun makeSounds(repeticiones: Int) {
        for(i in 0..repeticiones) {
            println("T${tono.repeat(3)}M")
        }
    }
}

fun main(args: Array<String>) {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}